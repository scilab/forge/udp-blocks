//
// This file is part of the UDP blocks toolbox
//
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// see license.txt for more licensing informations

function [x,y,typ]=UDP_SEND(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    standard_draw(arg1)
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1)
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1)
   case 'getorigin' then
    [x,y]=standard_origin(arg1)
   case 'set' then
    x=arg1;
    // no parameters yet
    model=arg1.model;
    graphics=arg1.graphics;
    values=graphics.exprs;
    while %t do
      [ok, hostNameExpr, hostPortExpr, newValues]=scicos_getvalue("Set UDP_SEND block parameters", ...
      [_("Hostname"); _("Port")], ...
      list('str',1,'vec',1), ...
      values)
      if ~ok then
        break
      end
      //
      // Port
      //
      if hostPortExpr < 1024
        message("Port number must be greater than 1024.");
        ok=%f;
      else
         hostPort = hostPortExpr;
      end
      // Check hostNameExpr is not a scilab variable.
      if ok
        if exists(hostNameExpr)
          hostName = evstr(hostNameExpr)
        else
           hostName = hostNameExpr;
        end
      end


      if ok then
        model.ipar = hostPort;
        model.opar = list(hostName);
        graphics.exprs = newValues;
        arg1.graphics = graphics;
        arg1.model = model;
        x=arg1;
        break
      end
    end
   case 'define' then
    model=scicos_model()
    model.sim=list('udp_send',4)
    model.in=1
    model.intyp=1
    model.blocktype='c'
    model.dep_ut=[%f %t]
    model.ipar = 8888;
    model.opar = list("localhost")
    exprs=["localhost"; "8888"]
    gr_i=[];
    x=standard_define([2 2],model,exprs,gr_i)
    x.graphics.style=["blockWithLabel;verticalLabelPosition=bottom;verticalAlign=top;spacing=0;displayedLabel=Hostname: %s<br>Port: %s"];
  end
endfunction

